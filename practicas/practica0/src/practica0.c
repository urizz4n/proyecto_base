#include <stdio.h>
#include <stdlib.h>

int ejercicio1();
int ejercicio2(int lista[]);
int ejercicio3(int lista[]);
double ejercicio4(int numerouno, int numerodos, int numerotres);
char ejercicio5();
int ejercicio6(int numero);
void ejercicio7();

int main() {
	ejercicio1();

	//int lista[] = {2,3,1,3,4,10,5,6,7,8,9,2};
	
	//ejercicio2(lista);

	//ejercicio3(lista);

	//ejercicio4(1,4,5);

	//ejercicio5();

	//ejercicio6(2);

	//ejercicio7();

	return 0;
}

int ejercicio1() {
	
	char * nombre = "Juan";
	int edad = 34;
	
	printf("Su nombre es: %s\nSu edad es: %i\n", nombre,edad);
	return 0;
}


int ejercicio2(int lista[]) {
	int maximo = lista[0];
	int numeros = sizeof lista / sizeof lista[0];

	for (int i = 0; i < numeros; i++) {

		if(lista[i] > maximo) {
			maximo = lista[i];
		}
	}

	printf("el máximo es: %d", maximo);
	return maximo;
}

int ejercicio3(int lista[]) {
	int minimo = lista[0];
	int numeros = sizeof lista / sizeof lista[0];

	for (int i = 0; i < numeros; i++) {
		if(lista[i] < minimo) {
			minimo = lista[i];
		}
	}

	printf("el minimo es: %d", minimo);
	return minimo;
}

double ejercicio4(int numerouno, int numerodos, int numerotres) {
	double resultado = (double)(numerouno + numerodos + numerotres)/3;

	printf("Promedio %.2f\n", resultado);
	double resultado;
}


char ejercicio5() {
	int i;

	for (i = 0; i < 128; i++) {
		printf("%c\n", i);
	}
}

int ejercicio6(int numero) {
	if(numero % 2 == 0) {
		printf("%d numero par ingresado", numero);
	} else {
		printf("%d numero impar ingresado", numero);
	}
	
	return 0;
}

void ejercicio7() {
	
	//Reconozco que pedí ayuda en este punto
	
	int opcion;

	while(opcion != 4){
		printf("1. Opcion 1\n");
		printf("2. Opcion 2\n");
		printf("3. Opcion 3\n");
		printf("4. Salir\n");
		printf("Ingrese opcion: ");

		scanf("%d", &opcion);

		switch(opcion){
			case 1:
				printf("Elegiste opcion 1\n\n");
				break;
			case 2:
				printf("Elegiste opcion 2\n\n");
				break;
			case 3:
				printf("Elegiste opcion 3\n\n");
				break;
			case 4:
				printf("Fin\n\n");
				break;
		}
	}

}

