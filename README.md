# Máquina virtual para prácticas de la materia Algoritmos III

Este repositorio sirve como proyecto base para llevar adelante las prácticas de Algoritmos III de la Universidad de Tres de Febrero para el 1er. cuatrimestre de 2020.

Como primer paso se debe hacer un fork de el repositorio a su propia cuenta y clonarlo localmente.

## Instruccciones de uso

Para tener un ambiente estandar donde compilar el código y ejecutarlo, utilizaremos una máquina virtual corriendo Linux (Ubuntu 18.04).

### Prerequisitos

Para ejecutar dicha VM utilizaremos dos herramientas que deben ser instaladas como prerequisito:

* **VirtualBox:** provee la virtualización propiamente dicha. Puede ser descargado desde https://www.virtualbox.org/wiki/Downloads
* **Vagrant:** simplifica la creación y ejecución de la máquina virtual, instalando los paquetes necesarios para poder compilar el código C / C++. Puede ser descargado desde https://www.vagrantup.com/downloads.html.

### Creación de la máquina virtual

Una vez instaladas las aplicaciones, desde la línea de comandos se debe cambiar al directorio donde este repositorio fue clonado y ejecutar el comando `vagrant up`. Una vez que haya finalizado se puede ejecutar el comando `vagrant status`, que debería mostrar una salida similar a la siguiente:

![salida vagrant status](imgs/salida_vagrant_status.png)

Esto quiere decir que la máquina virtual está corriendo correctamente.

### Compilando y ejecutando el código

Para poder compilar el código desde la máquina virtual debemos primero ingresar "remotamente", para ello utilizamos el comando `vagrant ssh`. La salida por pantalla indicando que estamos dentro de la máquina virtual debería ser la siguiente:

![salida vagrant ssh](imgs/salida_vagrant_ssh.png)

Luego debemos cambiar al directorio `/home/vagrant/practicas` cuyo contenido es automáticamente sincronizado con el directiorio `practicas` de este repositorio. Para confirmar que podemos compilar y ejecutar el código podemos utilizar el ejemplo en la `practica0`:

```shell
cd practicas/practica0/src
gcc hola_mundo.c -o hola_mundo
./hola_mundo
```

La salida debería ser:

![salida hola_mundo](imgs/salida_hola_mundo.png)
